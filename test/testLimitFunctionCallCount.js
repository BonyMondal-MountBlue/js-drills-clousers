const {limitFunctionCallCount} = require("../limitFunctionCallCount");
function cb() {
    console.log("callback invoked");
}
let times=limitFunctionCallCount(cb,2)
times.invoke()
times.invoke()
times.invoke()
times.invoke()