module.exports = {
    // Should return a function that invokes `cb`.
    // The returned function should only allow `cb` to be invoked `n` times.
    // Returning null is acceptable if cb can't be returned
    limitFunctionCallCount: function (cb, n) {
        let count = n;
        function invoke() {
            if (count > 0) {
                cb();
                count--;
            }
            else {
                console.log(null)
            }
        }
        return { invoke };
    }
}